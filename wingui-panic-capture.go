package main

import (
	"fmt"
	"log"
	"os"
	"syscall"
)

var (
	kernel32         = syscall.MustLoadDLL("kernel32.dll")
	procSetStdHandle = kernel32.MustFindProc("SetStdHandle")
)

func SetStdHandle(stdhandle int32, handle syscall.Handle) error {
	r0, _, e1 := syscall.Syscall(procSetStdHandle.Addr(), 2, uintptr(stdhandle), uintptr(handle), 0)
	if r0 == 0 {
		if e1 != 0 {
			return error(e1)
		}
		return syscall.EINVAL
	}
	return nil
}

func execute() {
	f, err := os.Create(`c:\a.txt`)
	if err != nil {
		log.Fatalf("os.Create failed: %v", err)
	}
	//defer f.Close()

	err = SetStdHandle(syscall.STD_ERROR_HANDLE, syscall.Handle(f.Fd()))
	if err != nil {
		log.Fatalf("SetStdHandle failed: %v", err)
	}

	panic("panicking")
}

func main() {
	execute()
	fmt.Printf("Should not return here\n")
	os.Exit(1234)
}
